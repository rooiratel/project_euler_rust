pub fn run() {
    let mut largest_palindrome = 0;

    let mut product;

    let mut x = 100;

    while x < 1000 {
        let mut y = 100;

        while y < 1000 {
            product = x * y;

            if is_palindrome(product) {
                if product > largest_palindrome {
                    largest_palindrome = product;
                }
            }

            y = y + 1;
        }

        x = x + 1;
    }

    println!(
        "{} is the largest product of two 3 digit numbers that is a palindrome",
        largest_palindrome
    );
}

fn is_palindrome(x: i32) -> bool {
    let s: String = x.to_string();

    let s_rev: String = s.chars().rev().collect();

    s == s_rev
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn true_01() {
        assert_eq!(true, is_palindrome(2002));
    }

    #[test]
    fn true_02() {
        assert_eq!(true, is_palindrome(1122332211));
    }

    #[test]
    fn true_03() {
        assert_eq!(true, is_palindrome(202));
    }

    #[test]
    fn false_01() {
        assert_eq!(false, is_palindrome(201));
    }
}