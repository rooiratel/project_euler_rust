pub fn run(fxnn: i32) {
    println!(
        "For the first {} natural numbers, the difference of the 2 functions is {}",
        fxnn,
        diff(fxnn)
    );
}

fn diff(max: i32) -> i32 {
    square_of_sum(max) - sum_of_squares(max)
}

fn sum_of_squares(max: i32) -> i32 {
    let mut sum = 0;

    for x in 1..=max {
        sum = sum + (x * x);
    }
    sum
}

fn square_of_sum(max: i32) -> i32 {
    let mut sum = 0;
    for x in 1..=max {
        sum = sum + x;
    }

    sum * sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_01() {
        assert_eq!(385, sum_of_squares(10));
    }

    #[test]
    fn test_02() {
        assert_eq!(3025, square_of_sum(10));
    }

    #[test]
    fn test_03() {
        assert_eq!(2640, diff(10));
    }
}
