pub fn run(nat_max: i32) {
    println!("The sum of the multiples of all natural numbers below {} is {}", nat_max, mul_sum(nat_max));
}

fn mul_sum(max_val: i32) -> i32 {
    let mut sum = 0;

    for num in 1..max_val {
        if (num % 3 == 0) || (num % 5 == 0) {
            sum = sum + num;
        }
    }

    sum
}

#[cfg(test)]
mod tests {
    use super::*; // ???

    #[test]
    fn test_max_10() {
        assert_eq!(23, mul_sum(10));
    }

    #[test]
    fn test_max_1000() {
        assert_eq!(233168, mul_sum(1000));
    }
}