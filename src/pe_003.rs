pub fn run(num: u64) {
    println!("The largest prime factor for the number {} is {}", num, get_largest_prime_factor_for_num(num));
}

fn get_largest_prime_factor_for_num(num: u64) -> u64 {
    let mut n = num;

    let mut factor = 2;

    let mut largest_factor = 2;

    while n > 1 {
        if n % factor == 0 {
            largest_factor = factor;

            n = n / factor;
        } else {
            factor = factor + 1;
        }
    }

    largest_factor
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_13195() {
        assert_eq!(29, get_largest_prime_factor_for_num(13195));
    }

    #[test]
    fn test_600851475143() {
        assert_eq!(6857, get_largest_prime_factor_for_num(600851475143));
    }
}

