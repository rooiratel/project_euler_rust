pub fn run(max_term_val: u32) {
    println!(
        "The sum of all the even valued terms in the Fibonacci sequence up to the value of {} is {}",
        max_term_val,
        sum_even_fib(max_term_val)
    );
}

fn sum_even_fib(max_term_val: u32) -> u32 {
    let mut sum: u32 = 0;

    let fib_seq = get_fib_seq_up_to_max_val(max_term_val);

    for i in &fib_seq {
        if i % 2 == 0 {
            sum = sum + i;
        }
    }

    sum
}

// function that returns a vector containing fib seq terms up to max val

fn get_fib_seq_up_to_max_val(max_term_val: u32) -> Vec<u32> {
    let mut fib_vec: Vec<u32> = Vec::new();

    let mut penult_val: u32 = 1;

    let mut last_val: u32 = 1;

    while last_val < max_term_val {
        let tmp: u32 = last_val;

        last_val = last_val + penult_val;

        penult_val = tmp;

        fib_vec.push(last_val);
    }

    fib_vec
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_max_80() {
        assert_eq!(44, sum_even_fib(80));
    }

    #[test]
    fn test_max_4000000() {
        assert_eq!(4613732, sum_even_fib(4000000));
    }
}