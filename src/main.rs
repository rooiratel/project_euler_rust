mod pe_001;
mod pe_002;
mod pe_003;
mod pe_004;
mod pe_005;
mod pe_006;
mod pe_007;

fn main() {
    println!("Project Euler problem 1 :");
    pe_001::run(1000);
    println!("\nProject Euler problem 2 :");
    pe_002::run(4000000);
    println!("\nProject Euler problem 3 :");
    pe_003::run(600851475143);
    println!("\nProject Euler problem 4 :");
    pe_004::run();
    println!("\nProject Euler problem 5 :");
    pe_005::run();
    println!("\nProject Euler problem 6 :");
    pe_006::run(100);
    println!("\nProject Euler problem 7 :");
    pe_007::run(10001);
    //println!("\nProject Euler problem 8 :");
    //pe_008::run(10001);
}
