// this solution needs runtime optimization

pub fn run() {
    let mut answer_found = false;
    let mut number = 1;
    let upper_bound = 20;

    while answer_found == false {
        if is_evenly_divisible_by_numbers_in_range(number, upper_bound) {
            println!(
                "{} is evenly divisible by all numbers from 1 to {}",
                number, upper_bound
            );
            answer_found = true;
        } else {
            number = number + 1;
        }
    }
}

fn is_evenly_divisible_by_numbers_in_range(num: u32, upper_bound: u32) -> bool {
    for x in 1..upper_bound {
        if num % x != 0 {
            return false;
        }
    }
    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_01() {
        assert_eq!(true, is_evenly_divisible_by_numbers_in_range(2520, 10));
    }
}
