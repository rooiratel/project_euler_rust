pub fn run(n: u32) {
    println!("The {} prime number is {}", n, nth_prime(n));
}

fn nth_prime(n: u32) -> u32 {
    let mut nth_prime = 0;
    let mut num = 1;

    while nth_prime != n {
        if is_prime(num) {
            nth_prime = nth_prime + 1;
        }
        if nth_prime != n {
            num = num + 1;
        }
    }
    num
}

fn is_prime(num: u32) -> bool {
    if num == 1 {
        return false;
    }
    if num == 2 {
        return true;
    }
    if num % 2 == 0 {
        return false; //quick cull
    }

    let max = (num as f64).sqrt() as u32;

    for n in (3..=max).step_by(2) {
        if num % n == 0 {
            return false;
        }
    }
    true
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn nth_prime_test_01() {
        assert_eq!(13, nth_prime(6));
    }

    #[test]
    fn is_prime_test_01() {
        assert_eq!(false, is_prime(1));
    }

    #[test]
    fn is_prime_test_02() {
        assert_eq!(true, is_prime(2));
    }

    #[test]
    fn is_prime_test_03() {
        assert_eq!(true, is_prime(3));
    }

    #[test]
    fn is_prime_test_04() {
        assert_eq!(false, is_prime(4));
    }

    #[test]
    fn is_prime_test_05() {
        assert_eq!(true, is_prime(5));
    }
}
