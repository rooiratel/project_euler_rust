# Project Euler Rust

This project contains my solutions to the [Project Euler](https://projecteuler.net/) problems using Rust. I will only include solutions from problem 1 to 100 as requested by the project. All other solutions I will keep in a separate private repo.

The solution to each problem is contained within its own module. I also include unit tests for each one.

main.rs just calls each module.